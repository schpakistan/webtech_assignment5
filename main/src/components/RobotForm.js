import React, { Component } from "react";

class RobotForm extends Component {
  constructor(props) {
    super(props);

    this.state = {
      name: "",
      type: "",
      mass: 0
    };

    this.handleOnNameChange = this.handleOnNameChange.bind(this);

    this.handleOnTypeChange = this.handleOnTypeChange.bind(this);

    this.handleOnMassChange = this.handleOnMassChange.bind(this);

    this.handleOnAdd = this.handleOnAdd.bind(this);
  }

  handleOnNameChange(event) {
    this.setState({ name: event.target.value });
  }

  handleOnTypeChange(event) {
    this.setState({ type: event.target.value });
  }

  handleOnMassChange(event) {
    this.setState({ mass: event.target.value });
  }

  handleOnAdd(event) {
    this.props.store.addRobot({
      name: this.state.name,
      type: this.state.type,
      mass: this.state.mass
    });
  }

  render() {
    return (
      <div>
        <label>
          Name:
          <input
            type="text"
            id="name"
            value={this.state.name}
            onChange={this.handleOnNameChange}
          ></input>
        </label>
        <label>
          Type:
          <input
            type="text"
            id="type"
            value={this.state.type}
            onChange={this.handleOnTypeChange}
          ></input>
        </label>
        <label>
          Mass:
          <input
            type="number"
            id="mass"
            value={this.state.mass}
            onChange={this.handleOnMassChange}
          ></input>
        </label>
        <br />
        <button value="add" onClick={this.handleOnAdd}>
          Add
        </button>
      </div>
    );
  }
}

export default RobotForm;
